#include <stdio.h>
#include <unistd.h>
#include "config.h"
#include "siparse.h"
#include "utils.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <builtins.h>
#include <fcntl.h>

int main(int argc, char *argv[]){
	char buffor[MAX_LINE_LENGTH+1];
        int for_childs[1024];
	struct stat buf;
	int do_prompt;
	int j = 0;
	int start = 0;
	int end = 0;
	int s_bad = 0;
	int stdin_copy = 0;
	int stdout_copy = 0;
        //counter on procceses which are foreground
        int counter = 0;
        int forg = 0;
	redirection** red;
	if(fstat(0, &buf) == 0){         
		do_prompt = S_ISCHR(buf.st_mode);
	}
	else{
		exit(1);
	}
	while(1){
		if(do_prompt){
			//Writing a PROMPT on Console
			while(1){
				int a = 0;
				int length = sizeof(PROMPT_STR);
				a += write(1,PROMPT_STR, length);
				if(a >= length)
					break;
			}
		}
		//if got 0 and it was file I quit
		if(end && !do_prompt) break;
		//translation of buffor
		for(int i = 0; i < j - start; i++){
			buffor[i] = buffor[start + i];
		}
		s_bad = 0;
		end = 0;
		j = j - start;
	    start = 0;
		int b = read(0, buffor + j, MAX_LINE_LENGTH - j);
		if(b == 0){
			break;
		}
		buffor[b + j] = 0;
		//going through buffor till end of him, end of file or just getting end of line(after that still goin)
		while(1){
			while(j < MAX_LINE_LENGTH && buffor[j] != '\n' && buffor[j] != 0)
				j++;
			if(j == MAX_LINE_LENGTH) break;
			if(buffor[j] == 0){end = 1;}
			buffor[j] = 0;
			int for_proc_count = 0;
			//Checking if this program is going to be done in for or bg
                        if(j - start >= 1){
				if(buffor[j-1] == '&'){
					forg = 1;
				}
				else{ 
					forg = 0;
				}
			}
			else{
				forg = 0;
			}
			line* ln = parseline(buffor + start);
			start = ++j;
			if( ln != NULL){
				command* com = pickfirstcommand(ln);
				if(com != NULL){
					//Checking if command is shelcom or program
					char* name = com->argv[0];
					red = com->redirs;
					for(int i = 0; i < 2; i++){
						if(red[i] != NULL){
							int flag = red[i]->flags;
							char *fname = red[i]->filename;
							int desc;
							if(IS_RIN(flag)){
								int desc = open(fname, O_RDWR);
								if(desc < 0){
									write(2,strcat(com->argv[0]," :no such file or directory\n"),40);
									break;
								}
								stdin_copy = dup(0);
								dup2(desc, 0);
							}
							if(IS_ROUT(flag)){
								int desc2 = open(fname, O_CREAT | O_TRUNC | O_RDWR );
								if(desc2 < 0){
									break;
								}
								stdout_copy = dup(1);
								dup2(desc2, 1);
							}
							if(IS_RAPPEND(flag)){
								int desc3 = open(fname, O_CREAT | O_TRUNC | O_RDWR );
								if(desc3 < 0){
									break;
								}
								stdout_copy = dup(1);
								dup2(desc3, 1);
							}
						}
					}
					if(com->argv[0] == NULL){
						s_bad = 1;
					}
					if(!s_bad){
						char* func = builtins_table[0].name;
						int iter = 0;
						while(func != NULL){
							if(strcmp(func,com->argv[0]) == 0){
								break;
							}
							iter++;
							func = builtins_table[iter].name;
						}
						if(func == NULL){
						//	exec program in child procces
							int childPid = fork();
							if(forg){
								for_childs[for_proc_count++] = childPid;
								printf("%d \n", for_childs[0]);
							}
							if(childPid < 0)
								exit(1);
							if(!childPid){
								int code = execvp(name,com->argv);
								switch(errno){
									case ENOENT:
										write(2,strcat(com->argv[0]," :no such file or directory\n"),40);
										exit(EXEC_FAILURE);
									case EACCES:
										write(2,strcat(com->argv[0]," :permisson denied\n"),40);
										exit(EXEC_FAILURE);
									case -1: 
										break;
									default:
										write(2,strcat(com->argv[0], " :exec error\n"),40);
										exit(EXEC_FAILURE);
								}			
							}
							else{
								//shellprocces waitng for his child exec
								int child = waitpid(-1, NULL, WNOHANG);
								printf("%d \n", child);
								for(int i = 0; i < for_proc_count; i++){
									if(for_childs[i] == child){
										counter--;
									}
								} 
							}
						}
						else{
							//trying to make a command
							int res = builtins_table[iter].fun(com->argv);
						}
					}
					
					if(stdin_copy != 0)
						dup2(stdin_copy, 0);
					if(stdin_copy != 1 )
						dup2(stdout_copy, 1);
				}
				}
				else{
					write(2,strcat(SYNTAX_ERROR_STR,"\n"),20);
				}
			// end means that we had completed reading file and just got 0
			if(end || s_bad) break;
		}
	}
}
